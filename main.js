const express = require("express")

const app = express()
app.use(express.json())
const port = 4000

app.get("/", (req, res) => {
    res.send("Welcome to the Workshop CATS app!")
})

app.listen(port, () => {
    console.log(`Workshop CATS app is running on port ${port}`)
})
